FROM node:alpine

RUN mkdir -p /home/boogle

COPY . /home/boogle

WORKDIR /home/boogle

# ENV CONTAINER_URL http://localhost
ENV CONTAINER_URL https://booglehero.herokuapp.com

RUN npm install
RUN cd client/boogle && npm install
WORKDIR /home/boogle/client/boogle/
RUN REACT_APP_CONTAINER_URL=$CONTAINER_URL npm run build
WORKDIR /home/boogle

ENV PORT 4000

EXPOSE 4000 

CMD ["npm", "run", "start:prod"]


