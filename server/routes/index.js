const path = require('path');

const controller = require('./../controller');


module.exports = app => {
    app.get('/', (req, res) => {
        res.sendFile(path.join(__dirname, '../../client/boogle/build/index.html'));
        // res.render('index.html',{ client: process.env.CLIENT_URL});
    });    
    app.use('/api', controller);
}