const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const path = require('path');
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('views', path.join( __dirname,'../', 'client'));
app.use(express.static(path.join(__dirname,'../','client','boogle', 'build')));


app.engine('html', ejs.renderFile);
app.set('view engine', 'html');

const server = {
    start : start = () => {
        const routes = require('./routes');
        const httpServer = http.createServer(app);
        try {
            console.log(process.env.PORT)
            httpServer.listen(process.env.PORT || 4999);
            routes(app);
            console.log( 'server running @ localhost:4999');
        } catch(err) {
            console.log('server could not start');
        }
    }
}

server.start();

