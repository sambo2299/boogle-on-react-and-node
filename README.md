**BOOBLE REACT + NODE**

**REQUIREMENTS**

    node - latest

**steps:**

    npm install -g mocha // to test node server
    cd [APP ROOTFOLDER]
    npm install
    cd client/boogle
    npm install
    cd [APP ROOTFOLDER]
    npm start

**to start start for server and client together** 
    npm test  

**to start server tests only**

    mocha server/tests/server.test.js

**to start client tests only**

    cd client/boogle
    npm test



**install and run from bash file** 

    start bash file start.sh 
    

**run on container using docker**

    install docker, docker-compose
    build docker image
    run docker container
    

