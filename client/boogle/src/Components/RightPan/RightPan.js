import React from 'react'

import classes from './RightPan.css';
import Timer from '../Timer/Timer';
import Button from '../Button/Button';
import Block from '../WordLists/WordLists';

const rightPan = (props) => {
    const playingBlock = props.playing ? 
    <div>
        <Button
            clicked={props.refresh}
            refresh="true">
            Refresh
        </Button>
        <Timer timer={props.timer}/>
        <Block id="wordboard"
            playing={props.playing}
            userWords={props.userWords}
        />
    </div> 
    : 
    <div>
        <h4 className={classes.RuleHeader}>Rules</h4>
            <ul className={classes.RuleLists}>
                <li key="1">Select a block size of your choice</li>
                <li key="2">Find word from using letters from the blocks, choose neighbouring letters only</li>
                <li key="3">collect maximum words in 2 min timer</li>
                <li key="4">See results after 2 mins</li>
            </ul>
    </div>
    return(
        <div className={classes.Block}>
        {playingBlock}
        {props.score && <Block id="scoreboard" 
            playing={props.playing}
            score={props.score}
            />}
    </div>
    )
}

export default rightPan;