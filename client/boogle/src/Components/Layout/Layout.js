import React from "react";

import Header from '../Header/Header';
import classes from './Layout.css';

const layout = (props) =>
    <div className={classes.MainContainer}>
        <Header/>
        {props.children}
    </div>


export default layout;